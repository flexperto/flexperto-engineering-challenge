export type Data = Archive[];

export type Archive = {
    conference: Conference;

    navigation: {
        events: NavigationEvent[];
    };
};

export type Conference = {
    id: string;
    tenant: string;
    participants: Participant[];
};

export type Participant = {
    id: string;
    email: string;
    name: string;
};

export type NavigationEvent = {
    target: string;
    createdAt: Date;
};
