# Engineering Challenge

## Context

We have a new requirement: Our customers ask us to **archive**
some of our application data (conference meta information, navigation data, and
the like), and send those archives to their servers.

Your task is to write a script that takes our recent archives (an array of JSON
objects), and send the individual archive records to one of our customers
servers.

Once this first task is done, we'll try to improve your solution, or come up
with additional challenges on-the-fly.

## Challenge

* Go to [this page](https://webhook.site), and generate a unique URL, which you
  use as the customer's server. Please also share this URL with the team.
* Write a script that takes the array of archive records, and sends them
  _individually_ and _sequentially_ to our customer's server.

## Developer Notes

Clone this repository, it should include everything you need. `data.json`
includes the actual data, an array of archive records. You can treat this file
as your "database".

If you choose to use TypeScript or NodeJS, we've prepared some boilerplate for
you:

* You are allowed to look up stuff. We don't expect you to do everything from
  memory.
* Run `npm ci` and `npm run start:ts` if you pick TypeScript, or `npm run
  start:node`, if you pick NodeJS.
* We've included `axios` as a dependency, it's up to you, if you want to use it,
  or any other dependency, for that matter.
* We've included `Data.ts`, which describes the types of the data.
